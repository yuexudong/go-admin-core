module gitee.com/yuexudong/go-admin-core/plugins/logger/logrus

go 1.14

require (
	gitee.com/yuexudong/go-admin-core v1.3.9
	github.com/sirupsen/logrus v1.8.0
)

replace gitee.com/yuexudong/go-admin-core => ../../../
