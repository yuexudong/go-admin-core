module gitee.com/yuexudong/go-admin-core/plugins/logger/zap

go 1.14

require (
	gitee.com/yuexudong/go-admin-core v1.3.9
	go.uber.org/zap v1.10.0
)

replace gitee.com/yuexudong/go-admin-core => ../../../
